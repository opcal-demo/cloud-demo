package opcal.cloud.demo.cd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
@EnableDiscoveryClient
public class CdConfigServerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(CdConfigServerApplication.class, args);
	}

}
