# cd-service-demo

## cd-service-a

run from root

```sh
./mvnw spring-boot:run -pl ./consule-demo/cd-service-demo/cd-service-a
```

run from current

```sh
../../mvnw spring-boot:run -pl cd-service-a
```

demo api

native api 

```sh
curl --location --request POST '127.0.0.1:10283/mock-cd-a-native/cd-a-calculate?x=88.91&y=9.2109'
```

cross-service api

```sh
curl --location --request POST '127.0.0.1:10283/mock-cd-a-cross/a-calculate-b2?x=100'
curl --location --request POST '127.0.0.1:10283/mock-cd-a-cross/a-calculate-b3?x=2.8&y=3.81'
```


## cd-service-b

run from root

```sh
./mvnw spring-boot:run -pl ./consule-demo/cd-service-demo/cd-service-b
```

run from current

```sh
../../mvnw spring-boot:run -pl cd-service-b
```

demo api

native api 

```sh
curl --location --request POST '127.0.0.1:10284/mock-cd-b-native/cd-b-calcul-2?x=11.98&y=13.81'
curl --location --request POST '127.0.0.1:10284/mock-cd-b-native/cd-b-calcul-3?x=2.8&y=3.81&z=9'
```

cross-service api

```sh
curl --location --request POST '127.0.0.1:10284/mock-cd-b-cross/b-calculate-a?x=99.76'
```