package opcal.cloud.demo.cd.b.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.cd.b.service.client.CdAApiService;

/**
 * mock cross-service API interface
 */
@RestController
@RequestMapping("/mock-cd-b-cross")
public class CdBCrossServiceController {

	private @Autowired CdAApiService cdAApiService;

	@PostMapping("/b-calculate-a")
	public double aCalculateB2(@RequestParam Double x) {
		Random xFactor = new Random(x.longValue());
		return cdAApiService.cdACalculate(x, xFactor.nextDouble());
	}

}
