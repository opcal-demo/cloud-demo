package opcal.cloud.demo.cd.b.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import opcal.cloud.demo.cd.b.service.client.fallback.factory.CdAApiServiceFallbackFactory;

@FeignClient(name = "cd-service-a", path = "/mock-cd-a-native", fallbackFactory = CdAApiServiceFallbackFactory.class)
public interface CdAApiService {

	@PostMapping("/cd-a-calculate")
	double cdACalculate(@RequestParam Double x, @RequestParam Double y);
}
