package opcal.cloud.demo.cd.b.service.client.fallback.factory;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;
import opcal.cloud.demo.cd.b.service.client.CdAApiService;

@Component
public class CdAApiServiceFallbackFactory implements FallbackFactory<CdAApiService> {

	@Override
	public CdAApiService create(Throwable cause) {
		return new CdAApiService() {

			@Override
			public double cdACalculate(Double x, Double y) {
				return Double.NaN;
			}
		};
	}

}
