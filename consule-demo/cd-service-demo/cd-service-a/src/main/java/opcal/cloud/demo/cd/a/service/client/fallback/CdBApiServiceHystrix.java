package opcal.cloud.demo.cd.a.service.client.fallback;

import org.springframework.stereotype.Component;

import opcal.cloud.demo.cd.a.service.client.CdBApiService;

@Component
public class CdBApiServiceHystrix implements CdBApiService {

	@Override
	public double cdBCalculate(Double x, Double y) {
		return Double.NaN;
	}

	@Override
	public double cdBCalculate(Double x, Double y, Double z) {
		return Double.NaN;
	}

}
