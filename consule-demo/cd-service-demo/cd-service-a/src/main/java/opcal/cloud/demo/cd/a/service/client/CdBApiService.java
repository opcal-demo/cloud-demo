package opcal.cloud.demo.cd.a.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import opcal.cloud.demo.cd.a.service.client.fallback.CdBApiServiceHystrix;

@FeignClient(name = "cd-service-b", path = "/mock-cd-b-native", fallback = CdBApiServiceHystrix.class)
public interface CdBApiService {

	@PostMapping("/cd-b-calcul-2")
	double cdBCalculate(@RequestParam Double x, @RequestParam Double y);

	@PostMapping("/cd-b-calcul-3")
	double cdBCalculate(@RequestParam Double x, @RequestParam Double y, @RequestParam Double z);
}
