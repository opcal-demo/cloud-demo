package opcal.cloud.demo.cd.a.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * the native API interfaces provided by service A
 */
@RestController
@RequestMapping("/mock-cd-a-native")
public class CdAOriginalController {

	@PostMapping("/cd-a-calculate")
	public double cdACalculate(@RequestParam Double x, @RequestParam Double y) {
		return Math.log(x) / Math.PI * Math.sin(y);
	}
}
