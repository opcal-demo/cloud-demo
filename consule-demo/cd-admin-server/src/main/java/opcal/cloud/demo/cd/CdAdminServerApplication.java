package opcal.cloud.demo.cd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@Configuration
@EnableAutoConfiguration
@EnableAdminServer
@SpringBootApplication
public class CdAdminServerApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(CdAdminServerApplication.class, args);
	}

}
