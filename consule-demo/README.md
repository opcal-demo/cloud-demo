# consule-demo

This is a serial of demos using consule as service registration.

## cd-config-server

run from root

```sh
./mvnw spring-boot:run -pl ./consule-demo/cd-config-server
```

run from current

```sh
../mvnw spring-boot:run -pl cd-config-server
```

## cd-gateway

run from root

```sh
./mvnw spring-boot:run -pl ./consule-demo/cd-gateway
```

run from current

```sh
../mvnw spring-boot:run -pl cd-gateway
```

## cd-zuul

run from root

```sh
./mvnw spring-boot:run -pl ./consule-demo/cd-zuul
```

run from current

```sh
../mvnw spring-boot:run -pl cd-zuul
```

## cd-service-demo

This is a serial of [mocking business demos](cd-service-demo/README.md).


## service with gateway api demo

### gateway

```sh
curl --location --request POST '127.0.0.1:10281/cd-a-s/mock-cd-a-native/cd-a-calculate?x=88.91&y=9.2109'
curl --location --request POST '127.0.0.1:10281/cd-a-s/mock-cd-a-cross/a-calculate-b2?x=100'
curl --location --request POST '127.0.0.1:10281/cd-a-s/mock-cd-a-cross/a-calculate-b3?x=2.8&y=3.81'

curl --location --request POST '127.0.0.1:10281/cd-b-s/mock-cd-b-native/cd-b-calcul-2?x=11.98&y=13.81'
curl --location --request POST '127.0.0.1:10281/cd-b-s/mock-cd-b-native/cd-b-calcul-3?x=2.8&y=3.81&z=9'
curl --location --request POST '127.0.0.1:10281/cd-b-s/mock-cd-b-cross/b-calculate-a?x=99.76'
```

### zuul

```sh
curl --location --request POST '127.0.0.1:10282/cd-a-s/mock-cd-a-native/cd-a-calculate?x=88.91&y=9.2109'
curl --location --request POST '127.0.0.1:10282/cd-a-s/mock-cd-a-cross/a-calculate-b2?x=100'
curl --location --request POST '127.0.0.1:10282/cd-a-s/mock-cd-a-cross/a-calculate-b3?x=2.8&y=3.81'

curl --location --request POST '127.0.0.1:10282/cd-b-s/mock-cd-b-native/cd-b-calcul-2?x=11.98&y=13.81'
curl --location --request POST '127.0.0.1:10282/cd-b-s/mock-cd-b-native/cd-b-calcul-3?x=2.8&y=3.81&z=9'
curl --location --request POST '127.0.0.1:10282/cd-b-s/mock-cd-b-cross/b-calculate-a?x=99.76'
```
