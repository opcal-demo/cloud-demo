package opcal.cloud.demo.zd.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
public class ZdZuulApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ZdZuulApplication.class, args);
	}

}