# zookeeper-demo

This is a serial of demos using zookeeper as service registration.

## zd-config-server

run from root

```sh
./mvnw spring-boot:run -pl ./zookeeper-demo/zd-config-server
```

run from current

```sh
../mvnw spring-boot:run -pl zd-config-server
```

## zd-gateway

run from root

```sh
./mvnw spring-boot:run -pl ./zookeeper-demo/zd-gateway
```

run from current

```sh
../mvnw spring-boot:run -pl zd-gateway
```

## zd-zuul

run from root

```sh
./mvnw spring-boot:run -pl ./zookeeper-demo/zd-zuul
```

run from current

```sh
../mvnw spring-boot:run -pl zd-zuul
```

## zd-service-demo

This is a serial of [mocking business demos](zd-service-demo/README.md).


## service with gateway api demo

### gateway

```sh
curl --location --request POST '127.0.0.1:10181/zd-a-s/mock-zd-a-native/zd-a-calculate?x=9.88&y=21.98'
curl --location --request POST '127.0.0.1:10181/zd-a-s/mock-zd-a-cross/a-calculate-b2?x=99'
curl --location --request POST '127.0.0.1:10181/zd-a-s/mock-zd-a-cross/a-calculate-b3?x=57834.33&y=354783.112'

curl --location --request POST '127.0.0.1:10181/zd-b-s/mock-zd-b-native/zd-b-calcul-2?x=23&y=97.22'
curl --location --request POST '127.0.0.1:10181/zd-b-s/mock-zd-b-native/zd-b-calcul-3?x=89&y=78.64&z=90.13'
curl --location --request POST '127.0.0.1:10181/zd-b-s/mock-zd-b-cross/b-calculate-a?x=99.2131213'
```

### zuul

```sh
curl --location --request POST '127.0.0.1:10182/zd-a-s/mock-zd-a-native/zd-a-calculate?x=9.88&y=21.98'
curl --location --request POST '127.0.0.1:10182/zd-a-s/mock-zd-a-cross/a-calculate-b2?x=99'
curl --location --request POST '127.0.0.1:10182/zd-a-s/mock-zd-a-cross/a-calculate-b3?x=57834.33&y=354783.112'

curl --location --request POST '127.0.0.1:10182/zd-b-s/mock-zd-b-native/zd-b-calcul-2?x=23&y=97.22'
curl --location --request POST '127.0.0.1:10182/zd-b-s/mock-zd-b-native/zd-b-calcul-3?x=89&y=78.64&z=90.13'
curl --location --request POST '127.0.0.1:10182/zd-b-s/mock-zd-b-cross/b-calculate-a?x=99.2131213'
```
