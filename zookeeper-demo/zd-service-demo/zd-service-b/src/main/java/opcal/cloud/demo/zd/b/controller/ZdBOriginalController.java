package opcal.cloud.demo.zd.b.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *	the native API interfaces provided by service A
 */
@RestController
@RequestMapping("/mock-zd-b-native")
public class ZdBOriginalController {

	@Value("${zd.b.calculate-factor:0.9}")
	private double factor;

	@PostMapping("/zd-b-calcul-2")
	public double zdBCalculate(@RequestParam Double x, @RequestParam Double y) {
		return Math.log(x) * Math.expm1(y) / factor;
	}

	@PostMapping("/zd-b-calcul-3")
	public double zdBCalculate(@RequestParam Double x, @RequestParam Double y, @RequestParam Double z) {
		Random zfactor = new Random(z.longValue());
		return Math.log(x) * Math.log(y) / zfactor.nextDouble();
	}

}
