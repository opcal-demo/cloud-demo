package opcal.cloud.demo.zd.b.service.client.fallback.factory;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;
import opcal.cloud.demo.zd.b.service.client.ZdAApiService;

@Component
public class ZdAApiServiceFallbackFactory implements FallbackFactory<ZdAApiService> {

	@Override
	public ZdAApiService create(Throwable cause) {
		return new ZdAApiService() {

			@Override
			public double zdACalculate(Double x, Double y) {
				return Double.NaN;
			}
		};
	}

}
