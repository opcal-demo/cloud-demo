package opcal.cloud.demo.zd.b.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import opcal.cloud.demo.zd.b.service.client.fallback.factory.ZdAApiServiceFallbackFactory;

@FeignClient(name = "zd-service-a", path = "/mock-zd-a-native", fallbackFactory = ZdAApiServiceFallbackFactory.class)
public interface ZdAApiService {

	@PostMapping("/zd-a-calculate")
	double zdACalculate(@RequestParam Double x, @RequestParam Double y);
}
