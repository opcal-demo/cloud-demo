package opcal.cloud.demo.zd.b.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.zd.b.service.client.ZdAApiService;

/**
 * mock cross-service API interface
 */
@RestController
@RequestMapping("/mock-zd-b-cross")
public class ZdBCrossServiceController {

	private @Autowired ZdAApiService zdAApiService;

	@PostMapping("/b-calculate-a")
	public double aCalculateB2(@RequestParam Double x) {
		Random xFactor = new Random(x.longValue());
		return zdAApiService.zdACalculate(x, xFactor.nextDouble());
	}

}
