# zd-service-demo

## zd-service-a

run from root

```sh
./mvnw spring-boot:run -pl ./zookeeper-demo/zd-service-demo/zd-service-a
```

run from current

```sh
../../mvnw spring-boot:run -pl zd-service-a
```

demo api

native api 

```sh
curl --location --request POST '127.0.0.1:10183/mock-zd-a-native/zd-a-calculate?x=9.88&y=21.98'
```

cross-service api

```sh
curl --location --request POST '127.0.0.1:10183/mock-zd-a-cross/a-calculate-b2?x=99'
curl --location --request POST '127.0.0.1:10183/mock-zd-a-cross/a-calculate-b3?x=57834.33&y=354783.112'
```


## zd-service-b

run from root

```sh
./mvnw spring-boot:run -pl ./zookeeper-demo/zd-service-demo/zd-service-b
```

run from current

```sh
../../mvnw spring-boot:run -pl zd-service-b
```

demo api

native api 

```sh
curl --location --request POST '127.0.0.1:10184/mock-zd-b-native/zd-b-calcul-2?x=23&y=97.22'
curl --location --request POST '127.0.0.1:10184/mock-zd-b-native/zd-b-calcul-3?x=89&y=78.64&z=90.13'
```

cross-service api

```sh
curl --location --request POST '127.0.0.1:10184/mock-zd-b-cross/b-calculate-a?x=99.2131213'
```