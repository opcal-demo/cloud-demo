package opcal.cloud.demo.zd.a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class ZdServiceAApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ZdServiceAApplication.class, args);
	}

}