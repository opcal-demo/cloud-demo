package opcal.cloud.demo.zd.a.service.client.fallback;

import org.springframework.stereotype.Component;

import opcal.cloud.demo.zd.a.service.client.ZdBApiService;

@Component
public class ZdBApiServiceHystrix implements ZdBApiService {

	@Override
	public double zdBCalculate(Double x, Double y) {
		return Double.POSITIVE_INFINITY;
	}

	@Override
	public double zdBCalculate(Double x, Double y, Double z) {
		return Double.NEGATIVE_INFINITY;
	}

}
