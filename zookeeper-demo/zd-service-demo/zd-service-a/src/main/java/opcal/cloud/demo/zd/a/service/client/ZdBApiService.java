package opcal.cloud.demo.zd.a.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import opcal.cloud.demo.zd.a.service.client.fallback.ZdBApiServiceHystrix;

@FeignClient(name = "zd-service-b", path = "/mock-zd-b-native", fallback = ZdBApiServiceHystrix.class)
public interface ZdBApiService {

	@PostMapping("/zd-b-calcul-2")
	double zdBCalculate(@RequestParam Double x, @RequestParam Double y);

	@PostMapping("/zd-b-calcul-3")
	double zdBCalculate(@RequestParam Double x, @RequestParam Double y, @RequestParam Double z);
}
