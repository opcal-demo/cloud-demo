package opcal.cloud.demo.zd.a.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *	the native API interfaces provided by service A
 */
@RestController
@RequestMapping("/mock-zd-a-native")
public class ZdAOriginalController {

	@PostMapping("/zd-a-calculate")
	public double zdACalculate(@RequestParam Double x, @RequestParam Double y) {
		return Math.log(x) / Math.log(y);
	}
}
