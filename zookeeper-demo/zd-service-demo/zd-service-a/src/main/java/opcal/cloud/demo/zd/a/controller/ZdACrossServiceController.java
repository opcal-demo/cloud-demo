package opcal.cloud.demo.zd.a.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.zd.a.service.client.ZdBApiService;

/**
 * mock cross-service API interface
 */
@RestController
@RequestMapping("/mock-zd-a-cross")
public class ZdACrossServiceController {

	private @Autowired ZdBApiService zdBApiService;

	@PostMapping("/a-calculate-b2")
	public double aCalculateB2(@RequestParam Long x) {
		Random xFactor = new Random(x);
		return zdBApiService.zdBCalculate(x.doubleValue(), xFactor.nextDouble());
	}

	@PostMapping("/a-calculate-b3")
	public double aCalculateB3(@RequestParam Double x, @RequestParam Double y) {
		Random xFactor = new Random(y.longValue());
		return zdBApiService.zdBCalculate(x, y, xFactor.nextDouble());
	}
}
