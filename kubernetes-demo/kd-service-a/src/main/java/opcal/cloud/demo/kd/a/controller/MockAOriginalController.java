package opcal.cloud.demo.kd.a.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.kd.a.dto.CalculatorSetting;

/**
 * the native API interfaces provided by service A
 */
@RestController
@RequestMapping("/mock-kd-a-native")
public class MockAOriginalController {

	private @Autowired CalculatorSetting calculatorSetting;

	@GetMapping("/ccsetting")
	public CalculatorSetting ccsetting() {
		return calculatorSetting;
	}
	
	@PostMapping("/a-calculate")
	public double calculate(@RequestParam Long x, @RequestParam Long y) {
		return (x * y - calculatorSetting.getXfactor()) * 1D / (calculatorSetting.getYfactor() + 1) + calculatorSetting.getZfactor();
	}
}
