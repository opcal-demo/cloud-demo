package opcal.cloud.demo.kd.a.dto;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ConfigurationProperties(prefix = "calculator")
@RefreshScope
public class CalculatorSetting {
	private int xfactor;
	private int yfactor;
	private int zfactor;
}
