package opcal.cloud.demo.kd.a.configuration;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

import opcal.cloud.demo.kd.a.dto.CalculatorSetting;

@Configuration
@ConfigurationPropertiesScan(basePackageClasses = { CalculatorSetting.class })
public class KdAConfiguration {

}
