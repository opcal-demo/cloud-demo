package opcal.cloud.demo.kd.a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
@SpringBootApplication
public class KdServiceAApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(KdServiceAApplication.class, args);
	}

}