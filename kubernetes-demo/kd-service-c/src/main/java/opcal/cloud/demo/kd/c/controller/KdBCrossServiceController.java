package opcal.cloud.demo.kd.c.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.kd.c.service.client.KdAApiService;

/**
 * mock cross-service API interface
 */
@RestController
@RequestMapping("/mock-kd-c-cross")
public class KdBCrossServiceController {

	private @Autowired KdAApiService kdAApiService;

	@PostMapping("/c-calculate-a")
	public double cCalculateA(@RequestParam Double x) {
		Double pow = Math.pow(x, 2);
		return kdAApiService.calculate(x.longValue(), pow.longValue());
	}

}
