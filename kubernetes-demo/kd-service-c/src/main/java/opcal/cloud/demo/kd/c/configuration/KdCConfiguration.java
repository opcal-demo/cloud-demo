package opcal.cloud.demo.kd.c.configuration;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationPropertiesScan()
public class KdCConfiguration {

}
