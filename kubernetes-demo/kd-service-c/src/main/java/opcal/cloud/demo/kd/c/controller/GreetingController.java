package opcal.cloud.demo.kd.c.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class GreetingController {

	@Value("${kd.service.c.greet.greeting}")
	private String greetingMessage;
	
	@GetMapping("/greeting")
	public String greeting() {
		return greetingMessage;
	}
}
