package opcal.cloud.demo.kd.c;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
public class KdServiceCApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(KdServiceCApplication.class, args);
	}

}