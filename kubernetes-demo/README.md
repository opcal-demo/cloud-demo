# kubernetes-demo

## Migration to kubernetes
Before I studied how to use Spring Cloud Kubernetes, If I would have a mission that is going to 
make my system (like other serial demo) migrating to kubernetes, how would I do. I think I would
like to keep the system running stable, then I would not refactor excessively, maybe I would build
the docker images, define each service for kubernetes, and refactor loading configuration file.
But Now, I would think about it is possible to use configmap instead of config server, or use 
kubernetes service definition instead of eureka, zookeeper, consule. 

## Development
This serial demos are using okteto CLI for development runtime. Because cloud-demo is maven module
project, so okteto manifests are in the directory under the root module(cloud-demo/okteto).
The files in the directory 'okteto-dev-kube-yml' are yaml file for development runtime e.g namespace
ConfigMap.

The directories and files that are like this (kd-service-x/k8s/kd-service-x.yaml) are not for debug runtime.

### About demo modules
kd-service-b and kd-service-c are same, that different between them is kd-service-b loadbalancer is ribbon, 
and kd-service-c is not.

## Okteto CLI
Okteto CLI is a amazing thing for development with kubernetes. Here is my experience using Okteto.

If I would have a service application named 'service x' that will be built up from zero, and will call
other services that are running on kubernetes such as service y or service z, using okteto is interesting.
In this case, after oketeo manifests are set up, you could debug with kubernetes after running command 'okteto up',
and you don't need to build docker image, okteto is like a proxy to connect your kubernetes cluster and local machine.

In another case, If you would have two services, 'service a' and 'service b' are built up from zero, and b will call a,
using okteto is not easy, because at this time okteto just like a proxy, even though okteto CLI will auto create 
kubernetes service, the service is not work. For 'service b' development mission, the part of the code that calling service a,
should be waited until 'service a' use a formal kubernetes deployment.
