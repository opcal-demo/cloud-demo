package opcal.cloud.demo.kd.b.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.kd.b.service.client.KdAApiService;

/**
 * mock cross-service API interface
 */
@RestController
@RequestMapping("/mock-kd-b-cross")
public class KdBCrossServiceController {

	private @Autowired KdAApiService kdAApiService;

	@PostMapping("/b-calculate-a")
	public double bCalculateA(@RequestParam Double x) {
		Double pow = Math.pow(x, 2);
		return kdAApiService.calculate(x.longValue(), pow.longValue());
	}

}
