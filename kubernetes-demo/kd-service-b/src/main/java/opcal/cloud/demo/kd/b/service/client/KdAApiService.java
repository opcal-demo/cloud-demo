package opcal.cloud.demo.kd.b.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "kd-service-a", path = "/mock-kd-a-native", url = "http://kd-service-a:10581")
public interface KdAApiService {

	@PostMapping("/a-calculate")
	double calculate(@RequestParam Long x, @RequestParam Long y);
}
