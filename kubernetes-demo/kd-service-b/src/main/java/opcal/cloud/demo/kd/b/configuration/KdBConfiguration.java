package opcal.cloud.demo.kd.b.configuration;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationPropertiesScan()
public class KdBConfiguration {

}
