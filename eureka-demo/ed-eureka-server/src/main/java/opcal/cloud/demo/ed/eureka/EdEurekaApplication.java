package opcal.cloud.demo.ed.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableEurekaServer
@SpringBootApplication
public class EdEurekaApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(EdEurekaApplication.class, args);
	}

}
