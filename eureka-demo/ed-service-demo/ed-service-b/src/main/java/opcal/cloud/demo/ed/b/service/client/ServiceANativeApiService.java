package opcal.cloud.demo.ed.b.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import opcal.cloud.demo.ed.b.service.client.fallback.factory.ServiceANativeApiServiceFallbackFactory;

@FeignClient(name = "ed-service-a", path = "/mock-ed-a-native", fallbackFactory = ServiceANativeApiServiceFallbackFactory.class)
public interface ServiceANativeApiService {

	@PostMapping("/appendSecret")
	String appendSecret(@RequestParam String prefix);

	@PostMapping("/a-calculate")
	double calculate(@RequestParam Long x, @RequestParam Long y);
}
