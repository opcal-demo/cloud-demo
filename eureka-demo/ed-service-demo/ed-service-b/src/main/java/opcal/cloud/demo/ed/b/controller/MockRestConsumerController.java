package opcal.cloud.demo.ed.b.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.ed.b.service.client.NodeRestSidecarApiService;

@RestController
@RequestMapping("/rest-sidecar-consumer")
public class MockRestConsumerController {

	private @Autowired NodeRestSidecarApiService nodeRestSidecarApiService;

	@GetMapping("/consume")
	public double consume() {
		Random random = new Random();
		return nodeRestSidecarApiService.restCalculate(random.nextDouble(), random.nextDouble());
	}
}
