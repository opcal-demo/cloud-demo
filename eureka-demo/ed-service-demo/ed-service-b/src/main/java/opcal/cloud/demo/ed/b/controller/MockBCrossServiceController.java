package opcal.cloud.demo.ed.b.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.ed.b.service.client.NodeServiceCApiService;
import opcal.cloud.demo.ed.b.service.client.ServiceANativeApiService;

/**
 * mock cross-service API interface
 */
@RestController
@RequestMapping("/mock-ed-b-cross")
public class MockBCrossServiceController {

	private @Autowired ServiceANativeApiService serviceANativeApiService;
	private @Autowired NodeServiceCApiService serviceCApiService;

	@GetMapping("/checkAEncrypt")
	public String checkAEncrypt() {
		return serviceANativeApiService.appendSecret("check-from-b");
	}

	@PostMapping("/calculate")
	public double calculate(@RequestParam Long x, @RequestParam Long y) {
		Random random = new Random();
		return serviceANativeApiService.calculate((long) Math.pow(x.doubleValue(), random.nextInt(10)), (long) Math.pow(y, random.nextInt(10)));
	}

	@GetMapping("/consumeNode")
	public double consumeNode() {
		Random random = new Random();
		return serviceCApiService.calculate(random.nextDouble(), random.nextDouble());
	}
}
