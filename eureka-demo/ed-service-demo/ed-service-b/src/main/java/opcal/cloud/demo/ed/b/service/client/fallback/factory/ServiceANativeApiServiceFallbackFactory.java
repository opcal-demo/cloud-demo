package opcal.cloud.demo.ed.b.service.client.fallback.factory;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;
import opcal.cloud.demo.ed.b.service.client.ServiceANativeApiService;

@Component
public class ServiceANativeApiServiceFallbackFactory implements FallbackFactory<ServiceANativeApiService> {

	@Override
	public ServiceANativeApiService create(Throwable cause) {
		return new ServiceANativeApiService() {
			
			@Override
			public double calculate(Long x, Long y) {
				return x + y;
			}
			
			@Override
			public String appendSecret(String prefix) {
				return prefix + "-fallback";
			}
		};
	}

}
