package opcal.cloud.demo.ed.b.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "ed-node-rest-sidecar")
public interface NodeRestSidecarApiService {

	@PostMapping("/calculate")
	double restCalculate(@RequestParam Double x, @RequestParam Double y);
}
