package opcal.cloud.demo.ed.b.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "ed-node-service-c", path = "/mock-ed-c-native")
public interface NodeServiceCApiService {

	@PostMapping("/calculate")
	double calculate(@RequestParam Double x, @RequestParam Double y);
}
