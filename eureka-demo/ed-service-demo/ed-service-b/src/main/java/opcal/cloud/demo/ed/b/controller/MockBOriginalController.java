package opcal.cloud.demo.ed.b.controller;

import java.util.Random;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * the native API interfaces provided by service B
 */
@RestController
@RequestMapping("/mock-ed-b-native")
public class MockBOriginalController {

	@PostMapping("/b-calculate")
	public double bcalculate(@RequestParam Long x, @RequestParam Long y, @RequestParam Long z) {
		Random random = new Random();
		return Math.pow(x * y, random.nextInt(10)) * z;
	}
}
