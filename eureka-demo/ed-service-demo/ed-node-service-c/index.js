import eurekaUtils from './eurekaUtils.js';
import express from 'express';
import axios from 'axios';

eurekaUtils.init()

const app = express();

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
    console.log("ed-node-service-c on " + PORT);
})

app.get('/', (req, res) => {
    return res.send('ed-node-service-c api');
});

app.get('/health', (req, res) => {
    res.set('Content-Type', 'application/json');
    return res.send({
        "status": "UP"
    });
});

app.get('/info', (req, res) => {
    res.set('Content-Type', 'application/json');
    return res.send({});
});

app.post('/mock-ed-c-native/calculate', (req, res) => {
    let x = req.query.x;
    let y = req.query.y;
    let result = Math.sin(x) + Math.log2(y);
    console.log('sin(' + x + ") + log2(" + y + ")=" + result);
    res.set('Content-Type', 'application/json');
    return res.send(result + '');
});

app.get('/mock-ed-c-cross/checkAEncrypt', (req, res) => {

    const instances = eurekaUtils.eurekaClient.getInstancesByAppId('ed-service-a');

    if (!instances || instances.length == 0) {
        res.send('no host to ed-service-a');
    }

    let url = `http://${instances[0].ipAddr}:${instances[0].port['$']}/mock-ed-a-native/appendSecret`;
    console.log(url)
    const params = new URLSearchParams();
    params.append('prefix', 'check-from-c');
    axios.post(url, params)
        .then(function (response) {
            res.send(response.data);
        })
        .catch(function (error) {
            console.log(error);
            res.send('api error');
        });

});
