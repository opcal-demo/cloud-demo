import loadCloudConfig from './configLoader.js';
import ejclient from 'eureka-js-client';

const { Eureka } = ejclient;

const eurekaUtils = {
    eurekaClient: {}
};

eurekaUtils.init = function () {
    const currenthost = '127.0.0.1';
    const currentip = '127.0.0.1';
    const instanceId = currentip + ':' + process.env.application_name + ':' + process.env.PORT;
    const homePageUrl = 'http://' + currenthost + ':' + process.env.PORT;
    const statusPageUrl = homePageUrl + '/info';
    const healthCheckUrl = homePageUrl + '/health';
    loadCloudConfig(config => {
        const eurekaClient = new Eureka({
            instance: {
                app: process.env.application_name,
                hostName: currenthost,
                ipAddr: currentip,
                instanceId: instanceId,
                port: {
                    '$': process.env.PORT,
                    '@enabled': true
                },
                statusPageUrl: statusPageUrl,
                healthCheckUrl: healthCheckUrl,
                homePageUrl: homePageUrl,
                vipAddress: process.env.application_name,
                dataCenterInfo: {
                    '@class': 'com.netflix.appinfo.MyDataCenterInfo',
                    name: 'MyOwn'
                },
            },
            eureka: {
                serviceUrls: {
                    default: config.get('eureka.client.serviceUrl.defaultZone').split(',')
                },
                preferIpAddress: config.get('eureka.instance.prefer-ip-address')
            },
        });
        eurekaClient.logger.level('debug');
        eurekaClient.start(error => {
            console.log(error || process.env.application_name + " service registered");
        });

        eurekaClient.on('deregistered', () => {
            console.log('after deregistered');
            process.exit();
        })

        eurekaClient.on('started', () => {
            console.log("eureka start");
        })

        function exitHandler(options, exitCode) {
            if (options.cleanup) {
            }
            if (exitCode || exitCode === 0) console.log(exitCode);
            if (options.exit) {
                eurekaClient.stop();
            }
        }

        process.on('SIGINT', exitHandler.bind(null, { exit: true }));
        eurekaUtils.eurekaClient = eurekaClient;
    });
}


export default eurekaUtils;

