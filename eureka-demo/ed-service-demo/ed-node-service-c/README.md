# ed-node-service-c

This is a eureka service that is writting by node, and using 3rd lib ['eureka-js-client'](https://github.com/jquatier/eureka-js-client).

```sh
    const eurekaClient = new Eureka({
        instance: {
            app: process.env.application_name,
            hostName: currenthost,
            ipAddr: currentip,
            instanceId: instanceId,
            port: {
                '$': process.env.PORT,
                '@enabled': true
            },
            statusPageUrl: statusPageUrl,
            healthCheckUrl: healthCheckUrl,
            homePageUrl: homePageUrl,
            vipAddress: process.env.application_name,
            dataCenterInfo: {
                '@class': 'com.netflix.appinfo.MyDataCenterInfo', // spring cloud version = Hoxton.SR8 
                name: 'MyOwn'
            },
        },
        eureka: {
            serviceUrls: {
                default: config.get('eureka.client.serviceUrl.defaultZone').split(',')
            },
            preferIpAddress: config.get('eureka.instance.prefer-ip-address')
        },
    });
```