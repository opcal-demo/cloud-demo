import cloudConfigClient from 'cloud-config-client';
import dotenv from 'dotenv';

dotenv.config();

const options = {
    endpoint: process.env.cloud_config_uri,
    name: process.env.application_name,
    profiles: process.env.profiles_active
}

function loadCloudConfig(success) {
    cloudConfigClient.load(options).then(success);
}

export default loadCloudConfig;