# ed-service-demo

## ed-service-a

run from root

```sh
./mvnw spring-boot:run -pl ./eureka-demo/ed-service-demo/ed-service-a
```

run from current

```sh
../../mvnw spring-boot:run -pl ed-service-a
```

demo api

native api 

```sh
curl --location --request POST '127.0.0.1:10085/mock-ed-a-native/appendSecret?prefix=opal'
curl --location --request POST '127.0.0.1:10085/mock-ed-a-native/a-calculate?x=981238&y=833231'
```

cross-service api

```sh
curl --location --request POST '127.0.0.1:10085/mock-ed-a-cross/a-calculate-b?x=957'
```


## ed-service-b

run from root

```sh
./mvnw spring-boot:run -pl ./eureka-demo/ed-service-demo/ed-service-b
```

run from current

```sh
../../mvnw spring-boot:run -pl ed-service-b
```

demo api

native api 

```sh
curl --location --request POST '127.0.0.1:10086/mock-ed-b-native/b-calculate?x=8933&y=93238&z=712'
```

cross-service api

```sh
curl --location --request POST '127.0.0.1:10086/mock-ed-b-cross/calculate?x=183&y=877'
curl  '127.0.0.1:10086/mock-ed-b-cross/checkAEncrypt'
```

## Non-JVM service demo

## ed-node-rest-server

This is a non-jvm rest api server to simulate an existing API server, will not be refactored to join the spring cloud system. It's like an API service that has existed for a long time and written by node

run from root 

```sh
yarn  --cwd=./eureka-demo/ed-service-demo/ed-node-rest-server start
```

## ed-node-rest-sidecar

This is a sidecar project for ed-node-rest-server.

Sidecar project is like a proxy agent from non-jvm api service.


run from root

```sh
./mvnw spring-boot:run -pl ./eureka-demo/ed-service-demo/ed-node-rest-sidecar
```

run from current

```sh
../../mvnw spring-boot:run -pl ed-node-rest-sidecar
```

### consumes sidecar by service b

```sh
curl '127.0.0.1:10086/rest-sidecar-consumer/consume'
```

## ed-node-service-c
This is a non-JVM service demo writing by node, that is used 3rd ['eureka-js-client'](https://github.com/jquatier/eureka-js-client).

run from root 

```sh
yarn --cwd=./eureka-demo/ed-service-demo/ed-node-service-c start
```

### service c consums service a

```sh
 curl '127.0.0.1:10089/mock-ed-c-cross/checkAEncrypt'
```

### service b consums service c

```sh
curl '127.0.0.1:10086/mock-ed-b-cross/consumeNode'
```