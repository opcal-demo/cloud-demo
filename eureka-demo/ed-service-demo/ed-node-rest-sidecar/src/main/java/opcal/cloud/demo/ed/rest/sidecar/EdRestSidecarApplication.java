package opcal.cloud.demo.ed.rest.sidecar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.sidecar.EnableSidecar;

@EnableSidecar
@SpringBootApplication
public class EdRestSidecarApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(EdRestSidecarApplication.class, args);
	}

}