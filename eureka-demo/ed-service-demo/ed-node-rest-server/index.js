import express from 'express';
import dotenv from 'dotenv';

dotenv.config();

const app = express();

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
    console.log("ed-node-rest-server on " + PORT);
})

app.get('/', (req, res) => {
    return res.send('ed-node-rest-server api');
});

app.get('/health', (req, res) => {
    res.set('Content-Type', 'application/json');
    return res.send({
        "status": "UP"
    });
});

app.post('/calculate', (req, res) => {
    let x = req.query.x;
    let y = req.query.y;
    let result = Math.sin(x) + Math.log10(y);
    console.log('sin(' + x + ") + log10(" + y + ")=" + result);
    res.set('Content-Type', 'application/json');
    return res.send(result + '');
});
