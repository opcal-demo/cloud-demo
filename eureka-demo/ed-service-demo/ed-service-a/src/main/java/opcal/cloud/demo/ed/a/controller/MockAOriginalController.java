package opcal.cloud.demo.ed.a.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *	the native API interfaces provided by service A
 */
@RestController
@RequestMapping("/mock-ed-a-native")
public class MockAOriginalController {

	@Value("${ed.service.a.secret-data}")
	private String secret;

	@PostMapping("/appendSecret")
	public String appendSecret(@RequestParam String prefix) {
		return prefix + "-" + secret;
	}

	@PostMapping("/a-calculate")
	public double calculate(@RequestParam Long x, @RequestParam Long y) {
		Random xFactor = new Random(x);
		Random yFactor = new Random(y);
		return (x * y - xFactor.nextLong()) * 1D / (yFactor.nextLong() + 1);
	}
}
