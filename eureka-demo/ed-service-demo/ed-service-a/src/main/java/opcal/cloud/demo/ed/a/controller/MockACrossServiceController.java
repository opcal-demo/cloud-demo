package opcal.cloud.demo.ed.a.controller;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import opcal.cloud.demo.ed.a.service.client.ServiceBNativeApiService;

/**
 * mock cross-service API interface
 */
@RestController
@RequestMapping("/mock-ed-a-cross")
public class MockACrossServiceController {

	private @Autowired ServiceBNativeApiService serviceBNativeApiService;
	
	@PostMapping("/a-calculate-b")
	public double calculateWithSericeB(@RequestParam Long x) {
		Random xFactor = new Random(x);
		return serviceBNativeApiService.bcalculate(x, xFactor.nextLong(), x + xFactor.nextInt(x.intValue()));
	}
}
