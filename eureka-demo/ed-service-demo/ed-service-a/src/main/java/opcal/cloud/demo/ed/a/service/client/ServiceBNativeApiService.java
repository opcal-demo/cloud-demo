package opcal.cloud.demo.ed.a.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import opcal.cloud.demo.ed.a.service.client.fallback.ServiceBNativeApiServiceHystrix;

@FeignClient(name = "ed-service-b", path = "/mock-ed-b-native", fallback = ServiceBNativeApiServiceHystrix.class)
public interface ServiceBNativeApiService {

	@PostMapping("/b-calculate")
	double bcalculate(@RequestParam Long x, @RequestParam Long y, @RequestParam Long z);
}
