package opcal.cloud.demo.ed.a.service.client.fallback;

import org.springframework.stereotype.Component;

import opcal.cloud.demo.ed.a.service.client.ServiceBNativeApiService;

@Component
public class ServiceBNativeApiServiceHystrix implements ServiceBNativeApiService {

	@Override
	public double bcalculate(Long x, Long y, Long z) {
		return x + y - z;
	}

}
