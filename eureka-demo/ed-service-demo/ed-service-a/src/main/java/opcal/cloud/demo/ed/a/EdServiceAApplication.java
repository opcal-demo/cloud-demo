package opcal.cloud.demo.ed.a;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class EdServiceAApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(EdServiceAApplication.class, args);
	}

}