#ed-config-server

```sh
keytool -genkeypair -alias opcaldevkey -keyalg RSA \
  -dname "CN=Web Server,OU=Unit,O=Organization,L=City,S=State,C=US" \
  -keypass opcalse -keystore server.jks -storepass opcalse
```