# eureka-demo

This is a serial of demos using euerka as service registration.

## About config client

In this series of demos, in order to demonstrate configuration loading, I use two configuration methods,
one is using "spring.cloud.config.uri", and the other is using "spring.cloud.config.discovery.service-id".
On my experience of the last company, I prefer using "spring.cloud.config.uri".

## ed-config-server

run from root

```sh
./mvnw spring-boot:run -pl ./eureka-demo/ed-config-server 
```

run from current

```sh
../mvnw spring-boot:run -pl ed-config-server
```

## ed-eureka-server

Add the following entries on the host file.

```conf
127.0.0.1       peer1
127.0.0.1       peer2
```

### eureka1 port=10081
run from root module

```sh
./mvnw spring-boot:run -pl ./eureka-demo/ed-eureka-server -Dspring-boot.run.arguments="--SPRING_PROFILES=dev,peer1"
```

run from current module

```sh
../mvnw spring-boot:run -pl ed-eureka-server  -Dspring-boot.run.arguments="--SPRING_PROFILES=dev,peer1"
```

### eureka2 port=10082
run from root module

```sh
./mvnw spring-boot:run -pl ./eureka-demo/ed-eureka-server -Dspring-boot.run.arguments="--SPRING_PROFILES=dev,peer2"
```

run from current module

```sh
../mvnw spring-boot:run -pl ed-eureka-server -Dspring-boot.run.arguments="--SPRING_PROFILES=dev,peer2"
```

## ed-gateway

This is a gateway demo using spring gateway.

run from root module

```sh
./mvnw spring-boot:run -pl ./eureka-demo/ed-gateway
```

run from current module

```sh
../mvnw spring-boot:run -pl ed-gateway
```


## ed-zuul

This is a gateway demo using zuul.

run from root module

```sh
./mvnw spring-boot:run -pl ./eureka-demo/ed-zuul
```

run from current module

```sh
../mvnw spring-boot:run -pl ed-zuul
```

## ed-service-demo

This is a serial of [mocking business demos](ed-service-demo/README.md).

## service with gateway api demo

### gateway

```sh
curl --location --request POST '127.0.0.1:10083/a-s/mock-ed-a-native/appendSecret?prefix=opal'
curl --location --request POST '127.0.0.1:10083/a-s/mock-ed-a-native/a-calculate?x=981238&y=833231'
curl --location --request POST '127.0.0.1:10083/a-s/mock-ed-a-cross/a-calculate-b?x=957'

curl --location --request POST '127.0.0.1:10083/b-s/mock-ed-b-native/b-calculate?x=8933&y=93238&z=712'
curl --location --request POST '127.0.0.1:10083/b-s/mock-ed-b-cross/calculate?x=183&y=877'
curl  '127.0.0.1:10083/b-s/mock-ed-b-cross/checkAEncrypt'
```

### zuul

```sh
curl --location --request POST '127.0.0.1:10084/a-s/mock-ed-a-native/appendSecret?prefix=opal'
curl --location --request POST '127.0.0.1:10084/a-s/mock-ed-a-native/a-calculate?x=981238&y=833231'
curl --location --request POST '127.0.0.1:10084/a-s/mock-ed-a-cross/a-calculate-b?x=957'

curl --location --request POST '127.0.0.1:10084/b-s/mock-ed-b-native/b-calculate?x=8933&y=93238&z=712'
curl --location --request POST '127.0.0.1:10084/b-s/mock-ed-b-cross/calculate?x=183&y=877'
curl  '127.0.0.1:10084/b-s/mock-ed-b-cross/checkAEncrypt'
```

