# Spring cloud demo

Java Version 11

Spring Boot Version 2.3.9.RELEASE

Spring Cloud Version Hoxton.SR10

## Eureka serial demo
[Eureka Demo](eureka-demo/README.md) is a serial of demos using eureka service registration.

## Zookeeper serial demo
[Zookeeper Demo](zookeeper-demo/README.md) is a serial of demos using zookeeper service registration.

## Consule serial demo
[Consule Demo](consule-demo/README.md) is a serial of demos using consule service registration.

## Nacos serial demo
[Nacos Demo](https://gitlab.com/opcal-demo/alibaba-cloud-demo/-/tree/master/nacos-demo) is a serial of demos using nacos service registration.
Nacos demo is loacted in another demo project repository [alibaba-cloud-demo](https://gitlab.com/opcal-demo/alibaba-cloud-demo).

## Kubernetes serial demo
[Kubernetes Demo](kubernetes-demo/README.md) is a serial of demos using Spring Cloud Kubernetes.

## Configuration

### Brainstorming
When I was at my last company, my team development specification is to fetch the application's configuration from the config server;
the reason why it is designed this way is following;

1. let the configuration files(bootstrap.[yml | properties] or application.[yml | properties]) is the minimum for running, and these
configuration files should be packaged in JAR or WAR, that make all members of team to know that configuration is unmodified for deployment
 or application running, and stores the configuration that would be changed depending on environment changing to config server.

2. it is to avoid DevOps operator or developers additional modifications for configuration file in different deployment environmental changes,
 and we should deploy the application in different environment using a same script with different environment variables.

3. it is to increase the flexibility of the configuration. a simple application deployment environment specification is there naming, 
release, pre-release, test, dev; if we have a transient mission in pre-release environment, that is performance testing, avoiding the 
overall configuration of original pre-release's, then we just need to name a new configuration file like this
 'pre-release-performance.xx-application.yaml'. Another flexibility is to test in a specified environment configuration with different code branches.

Although both service discovery or URL can be used in the configuration of the Spring Config Client, the development specification in my
team is using URL way, which segment like following.

```properties
spring.cloud.config.uri=http://config-server/
```

According to the second point above, the configuration of service registration is a part of deployment environment, they should be fetched 
from config server, this is the reason why we make the specifications like that; so the config server is both a part of the 
micro-service system and also an independent external service system.

When writing this series of demos, I compared spring config server with those components with KV Storege function like zookeeper, consul, nacos, etc. 
These components have one thing that in common is the configuration of the config client that depends on the configuration of the service registration. 
According to my team's specification using them to become the config server is not a suitable choice;
second point of comparison is whether these have CLI Tools for KV Storege management, 
whether the management would increases the workload of DevOps operator or developers .

After I have studied using kubernetes, I rethink the specifications of the previous; 
if the application is developed with spring cloud kubernetes components, it is quite interesting to use kubernetes, 
which can replace the original service registration and use configmap to replace the original config server or other KV Storege components.


### Config Client Profile Division

On my experience of the last company, I would like to set up the configurations which are necessary for startup or which should not be fetched 
from config server in the bootstrap.[yml | properties].

A sample bootstrap.yml like this.

```yml
server:
  port: 8888
  
spring:
  application:
    name: demo-service
  profiles:
    active: ${SPRING_PROFILES:dev}
  cloud:
    config:
      fail-fast: true
      uri: http://127.0.0.1:10080/
      
feign:
  hystrix:
    enabled: true
    
logging:
  level:
    com.netflix.discovery.shared.resolver.aws.ConfigClusterResolver: ERROR
```

And I would like to use the profile name to distinguish the environment configuration, and store in config server. 
In this file will be setting up some configuration of business, service discovery, datasource etc.

A dev config like this.
demo-service-dev.yml

```yml
# service demo configuration
demo:
  config:
    system:
      some-settings:
      - a
      - b
    business:
      some-settings:
      - a
      - b
---
# eureka demo configuration
eureka:
  client:
    serviceUrl:
      defaultZone: http://peer1:18880/eureka/,http://peer2:18880/eureka/
  instance:
    instance-id: ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}
    prefer-ip-address: true

---
# zookeeper demo configuration
spring:
  cloud:
    zookeeper:
      connect-string: 127.0.0.1:2181

---
# consul demo configuration
spring:
  cloud:
    consul:
      host: 127.0.0.1
      port: 8500 


```

### Config Server Exposure

I prefer using "spring.cloud.config.uri" for config client. So I would like to using two ways to expose config server services, 
one is create a config server cluster, the other is using gateway services and service discovery to call config server services.

#### By Config Server Cluster

```mermaid
graph LR;

d([domain]) --> v(VIP)
v --> Config-Server-1
v --> Config-Server-2
v --> Config-Server-3

subgraph Config Server Cluster
Config-Server-1
Config-Server-2
Config-Server-3
end

```

#### By Gateway Cluster

```mermaid
graph LR;

d([domain]) --> v(VIP)
v --> Gateway-1
v --> Gateway-2
v --> Gateway-3  

Gateway-1 -.-> sd{{Service-Discovery}}
Gateway-2 -.-> sd
Gateway-3 -.-> sd

sd -.-> Config-Server-1
sd -.-> Config-Server-2
sd -.-> Config-Server-3

subgraph Gateway Cluster
Gateway-1
Gateway-2
Gateway-3
end

subgraph Config Server Services
Config-Server-1
Config-Server-2
Config-Server-3
end

```
